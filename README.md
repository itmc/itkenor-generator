#### 本地部署

> 部署流程
- 通过`git clone https://gitee.com/itkenor/itkenor-generator.git`下载源码
- 根据自己需求修改`resources/template`路径下的生成模板
- 修改`application.yml`中数据库相关信息
- IDEA运行`GeneratorApplication.java`，则可启动项目
- 项目访问路径：`http://localhost:8080`

#### 更新
> 2019-12-23号更新 `v2.8.0`
- Spring Boot 升级至最新的`2.2.2.RELEASE`
- Mybatis 升级至 `2.1.0`版
- Druid 升级至 `1.1.20`版
- FastJson 升级至 `1.2.62`版
- 优化页面展示布局

#### 启动
> 访问 localhost:8080 

![项目启动图](https://gitee.com/itkenor/itkenor-generator/raw/master/image/60f7a83d654012f63ef6b517763da61.png)
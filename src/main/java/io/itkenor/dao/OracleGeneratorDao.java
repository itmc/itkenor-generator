package io.itkenor.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * Oracle代码生成器
 *
 * @author Itkenor
 * @since 2019-08-27
 */
@Mapper
public interface OracleGeneratorDao extends GeneratorDao {

}

package io.itkenor.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * PostgreSQL代码生成器
 *
 * @author Itkenor
 * @since 2019-08-27
 */
@Mapper
public interface PostgreSQLGeneratorDao extends GeneratorDao {

}
